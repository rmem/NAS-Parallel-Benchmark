#!/bin/bash

prefix=$(dirname $(realpath $0))
cd $prefix

# install dependencies
apt install build-essential gfortran hwloc numactl


echo Compiling applications
rm bin/* 2>/dev/null
mkdir bin 2>/dev/null
make suite
