#!/bin/bash

prefix=$(dirname $(realpath $0))
echo $prefix

cd $prefix

echo "lstopo reports:"
lstopo
mkdir logs

sleep_duration=30
NITER=5
NTHREADS=$(lstopo --only core|wc -l)
export GOMP_CPU_AFFINITY=`hwloc-calc --physical-output --intersect PU --no-smt all`
export OMP_NUM_THREADS=$NTHREADS

echo getting numa nodes
NUMA_NODES=$(numactl --show|grep membind|sed 's/membind: //'| sed 's/ $//' |sed 's/ /,/g')


for iter in $(seq $NITER) ;do
    for app in $(ls bin); do
        for mode in first_touch interleaved ; do
            echo "Running $app $mode #$iter"

            if [ "$mode" = interleaved ]; then
                numactl -i $NUMA_NODES "./bin/$app" > logs/${app}_${mode}_${iter}_stdout.log 2> logs/${app}_${mode}_${iter}_stderr.log
            elif [ "$mode" = first_touch ]; then
                "./bin/$app" > logs/${app}_${mode}_${iter}_stdout.log 2> logs/${app}_${mode}_${iter}_stderr.log
            fi
            grep "Time in" logs/${app}_${mode}_${iter}_std*.log

	    sleep $sleep_duration
        done
     done       
done
